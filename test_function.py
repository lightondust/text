def sum_two_variable(x,y):
    return x+y

def add_two(x, function):
    return function(x,2)

print(add_two(9,sum_two_variable))
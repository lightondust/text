import matplotlib.pyplot as plt
import math
import codecs

#file_name = "hongloumeng_4268_counts.txt"
file_name = "shuihuzhuan_3601_counts.txt"

file_o = codecs.open(file_name,"r","utf-8")
file_t = file_o.readlines()
file_o.close()

y = []
minimum = 0
maximum = 500
for roll in range(minimum, maximum):
     y.append(math.log(int(file_t[roll].replace("\r","").split(":")[1])))
#     y.append(math.log(math.log(int(file_t[roll].replace("\r","").split(":")[1]))))
#     y.append(math.log(math.log(math.log(int(file_t[roll].replace("\r","").split(":")[1])))))

# for line in file_t:
#     y.append(math.log(int(line.replace("\r","").split(":")[1])))

x=[x_v for x_v in range(minimum, maximum)] 

plt.plot(x,y)   


plt.xlabel("x:{}-{}".format(str(minimum),str(maximum)))
plt.ylabel("Log(Log(Log Y))")
plt.title(file_name.split(".")[0])
#plt.legend()

plt.show()
from article import Article
import os

class Service:

    def __init__(self):
        pass

    def count_all(self):
        files = os.listdir(Article.DIR)
        for file in files:
            Article(file).count_character_set(to_file=True)

    def paragraph(self):
        files = os.listdir(Article.DIR)
        for file in files:
            Article(file).to_paragraph()
        
import codecs
import os

class Article():
    """
    ファイルから文章を読み込み、操作する、
    デフォルト設定ではtxtファイルは「file」フォルダに格納する
    """

    DIR = "file"
    DIR_COUNT="count"
    DIR_PARAGRAPH = "paragraph"
    DIR_SEARCH = "search"

    def __init__(self, file, cd="utf-8"):
        """
        self.contents: ファイルの内容が一行ずつ入っているリスト
        self.code: 文字コード、デフォルトはutf-8
        self.count_character_list()文字を数え上げ、出現回数の多い順でtupleを返す
        self.file_name: ファイル名
        """

        self.dir = Article.DIR
        self.dir_count = Article.DIR_COUNT
        self.dir_paragraph = Article.DIR_PARAGRAPH
        self.dir_search = Article.DIR_SEARCH

        self.code = cd
        self.file_name = file
        self.contents = self.read_from(file)



    def read_from(self, FILE=""):
        """
        ファイルから文字を読み取って返す
        """
        if not FILE:
            FILE = self.file_name
        
        FILE = os.path.join(self.dir, FILE)

        file_o = codecs.open(FILE, "r", self.code)
        file_t = file_o.readlines()
        file_o.close()

        return file_t


    def write_to(self, file_t="", FILE="", mode="txt"):
        """
        ファイルに書き込み
        mode=txt：そのまま
        mode=html：<p>タグで挟む
        """

        if not file_t:
            file_t = self.contents
        if not FILE:
            FILE = self.file_name

        if mode == "txt":
            file_o = codecs.open(FILE, "w", self.code)
            for sentence in file_t:
                file_o.write(sentence)
            file_o.close()
                
        elif mode=="html":
            FILE = FILE.split(".")[0]+".html"
            file_o = codecs.open(FILE, "w", self.code)
            for sentence in file_t:
                file_o.write("<p>"+sentence+"</p>")
            file_o.close()
        
    def get_character_set(self, character_only=True):
        """
        文字一覧集合を返す
        """
        character_set = set()
        for line in self.contents:
            for roll in range(0,len(line)):
                character_set.add(line[roll])

        if character_only:
            lower_letter = set([chr(i) for i in range(97,97+26)])
            upper_letter = set([chr(i) for i in range(65, 65+26)])
            number = set([chr(i) for i in range(48, 48+10)])
            other_symbols = set("\r\n，。；[]﹛？「」Φ%()<>^+　	：:！$﻿#、 ．“”-—~_\"'`》《.,*/")
            character_set = character_set - lower_letter - upper_letter - number - other_symbols

        return character_set

    def count_character(self, CHAR):
        """
        ファイル内に入っている特定の文字の出現回数を数える
        """

        count=0
        for line in self.contents:
            for roll in range(0,len(line)):
                if CHAR == line[roll]:
                    count += 1

        return count

    def count_character_set(self, character_set=set(), to_file=False, dir=""):
        """
        ファイル内に入っている文字リストの出現回数一覧を出す、
        デフォルトは辞書形式を返す
        ファイル書き込みはファイル名を返す
        注記:このメソッドで全体の文字を数える場合、
        １、文字セットを得る、
        ２、文字セットに存在するかどうかを確認する
        が本来不要なので、時間短縮が可能
        """
        
        if not dir:
            dir = self.dir_count


        #リスト指定がなければ文字全体の回数を数える
        if not character_set:
            character_set = self.get_character_set()

        #辞書の初期化        
        character_counts = {}
        for char in character_set:
            character_counts[char] = 0

        #一行ずつ、各行一文字ずつ数える
        for line in self.contents:
            for roll in range(0,len(line)):
                if line[roll] in character_set:
                    character_counts[line[roll]] += 1

        #出現回数の多い順でソート
        character_counts = sorted(character_counts.items(),key=lambda x:-x[1])

        #ファイル書き込み
        if to_file:
            set_length = len(character_set)

            #書き込みのために文字列に変換
            character_counts_string = \
                [":".join((x[0],str(x[1])))+"\n" for x in character_counts] 

            #pathを作る
            file_name = os.path.join(dir, \
                self.file_name.split(".")[0] + "_{}_counts.txt".format(set_length)) 

            if not os.path.exists(dir):
                os.mkdir(dir)

            self.write_to(file_t=character_counts_string, FILE=file_name) 

            return file_name

        else:
            return character_counts

    def to_paragraph(self, suffix="_p", dir="", to_file=True):

        if not dir:
            dir = self.dir_paragraph

        new_p = True
        paragraphs = []
        i = -1

        for line in self.contents:
            """頭文字を読んで新しい段落かどうか判断する"""

            #空行かどうかを判断
            if line.startswith("\r") or line.startswith("\n"):
                new_p = True
                blank_line = True 
            else:
                blank_line = False
            
                #空行ではない場合、空文字か第Ｘ回になっていないか
                if line.startswith("　") or line.startswith(" "):
                    new_p = True
                elif line.startswith("第") and line.endswith("回"):
                    new_p = True

            #空行は何もしない、それ以外で新段落なら段落を新しくする、そうでない場合は追加。
            if blank_line:
                pass
            elif new_p:
                if i>0:
                    paragraphs[i] += "\r\n"
                paragraphs.append(line.strip())
                i+=1
            else:
                paragraphs[i] += line.strip() 

            #段落に----を含む場合、新段落である可能性が高い
            if not blank_line:
                if "-"*10 in line:
                    new_p = True
                else:
                    new_p = False

        if to_file:
            new_file = self.file_name.split(".")[0] + suffix + ".txt"
            if not os.path.exists(dir):
                os.mkdir(dir)
            path =os.path.join(dir, new_file)
            self.write_to(file_t=paragraphs, FILE=path)
            return new_file
        else:
            return paragraphs

    def word_in_paragraph(self, words=[], condition="and", dir="", suffix="_", to_file=True, mode="html"):
        if not dir:
            dir = self.DIR_SEARCH

        paragraphs = self.to_paragraph(to_file=False)
        paragraphs_selected=[]

        if suffix == "_":
            suffix += "_".join(words)+"_"+condition

        if condition == "and":
            find = True
            for paragraph in paragraphs:
                for word in words:
                    if word in paragraph:
                        find = True
                    else:
                        find = False
                        break
                if find:
                    paragraphs_selected.append(paragraph)

        elif condition == "or":
            for paragraph in paragraphs:
                for word in words:
                    if word in paragraph:
                        paragraphs_selected.append(paragraph)
                        continue

        if to_file:
            if not paragraphs_selected:
                return "not found"
            new_file = self.file_name.split(".")[0] + suffix + ".txt"
            if not os.path.exists(dir):
                os.mkdir(dir)
            path =os.path.join(dir, new_file)
            self.write_to(file_t=paragraphs_selected, FILE=path, mode=mode)
            return new_file

        else:
            return paragraphs_selected



    def replace_word(self, word, word_by=""):
        """
        ファイル内の文字を置き換える
        """

        for roll in range(0, len(self.contents)):
            self.contents[roll] = self.contents[roll].replace(word, word_by)

        return self.contents


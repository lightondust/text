import codecs
import os

class Trans_Hanzi:
    def __init__(self):
        self.fanti_to_jianti={}
        self.jianti_to_fanti={}

        fanti = 0
        jianti = 1
        
        path = os.path.dirname(__file__)

        file = codecs.open(os.path.join(path,"fanjian.txt"),"r","utf-8")
        line = file.readline().strip()
        while line:
            pair = line.split(":")
            if pair[fanti] in self.fanti_to_jianti:
                if pair[jianti] in self.fanti_to_jianti[pair[fanti]]:
                    pass
                else:
                    self.fanti_to_jianti[pair[fanti]].append(pair[jianti])
            else:
                self.fanti_to_jianti[pair[fanti]] = [pair[jianti]]

            if pair[jianti] in self.jianti_to_fanti:
                if pair[fanti] in self.jianti_to_fanti[pair[jianti]]:
                    pass
                else:
                    self.jianti_to_fanti[pair[jianti]].append(pair[fanti])
            else:
                self.jianti_to_fanti[pair[jianti]] = [pair[fanti]]

            line = file.readline().strip()

        file.close()

trans = Trans_Hanzi()

print(trans.fanti_to_jianti["雇"])

print(trans.jianti_to_fanti["雇"])


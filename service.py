from article import Article
import os
import subprocess

class Service:

    def __init__(self):
        pass

    def count_all(self):
        files = os.listdir(Article.DIR)
        for file in files:
            Article(file).count_character_set(to_file=True)

    def paragraph(self):
        files = os.listdir(Article.DIR)
        for file in files:
            Article(file).to_paragraph()

    def search(self, words, file_to_file=False, show=True):
        files = os.listdir(Article.DIR)
        paragraphs = []
        for file in files:
            article = Article(file)
            result = article.word_in_paragraph(words, to_file=False)
            if result:
                paragraphs.append(file)
                paragraphs.extend(result)

        file_name = "search_{}.html".format("_".join(words))
        path = os.path.join(Article.DIR_SEARCH, file_name)

        article.write_to(file_t=paragraphs, FILE=path, mode="html")

        subprocess.call(["db.bat", path])


import codecs

class Data():

    def __init__(self, file="", cont="", cd="utf-8"):
        self.code = cd

        if file:
            self.contents = self.read_from(file)
        if cont:
            self.contents = cont
            

    def read_from(self, FILE):
        """
        ファイルからデータを読み取る
        """

        file_o = codecs.open(FILE, "r", self.code)
        file_t = file_o.readlines()
        file_o.close()

        return file_t


    def parse_csv(self, seperator=":"):
        self.data = []
        for content in self.contents:
            self.data.append(content.split(":"))


    
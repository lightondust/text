import math
import matplotlib.pyplot as plt

x = [0.1*x_v for x_v in range(100)]

y = [1-math.exp(x_v) for x_v in x]


plt.plot(x,y)

plt.title("a-exp")
plt.show()